use crate::audio_deck::AudioDeck;
use crate::gui::GuiToAudioMessage;
use crate::jack_backend::JackNotification;
use basedrop::Owned;
use std::sync::mpsc;

#[cfg(target_arch = "x86")]
use std::arch::x86::{_MM_FLUSH_ZERO_ON, _MM_SET_FLUSH_ZERO_MODE};
#[cfg(target_arch = "x86_64")]
use std::arch::x86_64::{_MM_FLUSH_ZERO_ON, _MM_SET_FLUSH_ZERO_MODE};

pub enum AudioEngineToGUIMessage {
    ElapsedTime(f32),
    BufferSizeChanged(usize),
}

/// AudioEngine is the central entry point into the audio processing code.
/// When the audio backend API (currently only JACK) requests new buffers
/// of audio to pass to the audio interface, AudioEngine is responsible for
/// telling everything that generates audio to do so, then mixing the signals
/// into buffers that get passed to the audio backend (currently only
/// JackProcessHandler).
pub struct AudioEngine {
    pub buffer_main_out: Vec<f32>,
    pub buffer_headphones_out: Vec<f32>,
    decks: Option<Owned<Vec<AudioDeck>>>,
    pub channels_per_frame: usize,
    sample_rate: usize,
    jack_notification_rx: Option<mpsc::Receiver<JackNotification>>,

    to_gui_tx: rtrb::Producer<AudioEngineToGUIMessage>,
    from_gui_rx: rtrb::Consumer<GuiToAudioMessage>,
    playhead_main_frames: f32,
    playhead_headphones_frames: f32,
}

impl AudioEngine {
    pub fn new(
        to_gui_tx: rtrb::Producer<AudioEngineToGUIMessage>,
        from_gui_rx: rtrb::Consumer<GuiToAudioMessage>,
    ) -> AudioEngine {
        AudioEngine {
            buffer_main_out: Vec::<f32>::new(),
            buffer_headphones_out: Vec::<f32>::new(),
            decks: None,
            channels_per_frame: 2,
            sample_rate: 0,
            jack_notification_rx: None,
            to_gui_tx,
            from_gui_rx,
            playhead_main_frames: 0f32,
            playhead_headphones_frames: 0f32,
        }
    }

    pub fn process(&mut self) {
        assert_no_alloc::assert_no_alloc(|| {
            #[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
            unsafe {
                _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_ON);
            }

            if let Some(rx) = &self.jack_notification_rx {
                if let Ok(message) = rx.try_recv() {
                    match message {
                        JackNotification::SampleRateChanged(sample_rate) => {
                            self.sample_rate = sample_rate
                        }
                    }
                }
            }

            while self.from_gui_rx.slots() > 0 {
                match self.from_gui_rx.pop().unwrap() {
                    GuiToAudioMessage::AddDeck { deck, mut vec } => {
                        if let Some(decks) = &mut self.decks {
                            for _ in 0..decks.len() {
                                vec.push(decks.pop().unwrap());
                            }
                        }
                        vec.push(deck);
                        self.decks = Some(vec);
                    }
                    GuiToAudioMessage::LoadClip {
                        deck_index,
                        clip,
                        vec,
                    } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to load a clip to a deck that does not exist.");
                            deck.load_clip(clip, vec);
                        }
                    }
                    GuiToAudioMessage::VolumeChanged { deck_index, value } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to set volume of a deck that does not exist.");
                            deck.volume.value = value;
                        }
                    }
                    GuiToAudioMessage::HeadphonesChanged { deck_index, value } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks.get_mut(deck_index).expect(
                                "Tried to toggle headphones on a deck that does not exist.",
                            );
                            deck.headphones_enabled.value = value as usize as f32;
                        }
                    }
                    GuiToAudioMessage::ClipMoved {
                        deck_index,
                        clip_index,
                        diff_seconds,
                    } => {
                        if let Some(decks) = &mut self.decks {
                            let deck = decks
                                .get_mut(deck_index)
                                .expect("Tried to move a clip on a deck that does not exist.");
                            deck.seek_clip(clip_index, diff_seconds);
                        }
                    }
                }
            }

            for sample in self.buffer_main_out.iter_mut() {
                *sample = 0f32;
            }
            for sample in self.buffer_headphones_out.iter_mut() {
                *sample = 0f32;
            }

            if let Some(decks) = &mut self.decks {
                for deck in decks.iter_mut() {
                    deck.process(self.playhead_main_frames, self.playhead_headphones_frames);
                    for (i, &sample) in deck.buffer_main_out.iter().enumerate() {
                        self.buffer_main_out[i] += sample;
                    }
                    for (i, &sample) in deck.buffer_headphones_out.iter().enumerate() {
                        self.buffer_headphones_out[i] += sample;
                    }
                }
            }

            self.playhead_main_frames +=
                self.buffer_main_out.len() as f32 / self.channels_per_frame as f32;
            self.playhead_headphones_frames +=
                self.buffer_headphones_out.len() as f32 / self.channels_per_frame as f32;
            let _ = self.to_gui_tx.push(AudioEngineToGUIMessage::ElapsedTime(
                self.playhead_main_frames / self.sample_rate as f32,
            ));
        })
    }

    pub fn set_buffer_size(&mut self, buffer_size_frames: usize) {
        self.buffer_main_out
            .resize(buffer_size_frames * self.channels_per_frame, 0f32);
        self.buffer_headphones_out
            .resize(buffer_size_frames * self.channels_per_frame, 0f32);

        if let Some(decks) = &mut self.decks {
            for deck in decks.iter_mut() {
                deck.set_buffer_size(buffer_size_frames);
            }
        }

        let _ = self
            .to_gui_tx
            .push(AudioEngineToGUIMessage::BufferSizeChanged(
                buffer_size_frames,
            ));
    }

    pub fn set_jack_notification_rx(&mut self, rx: mpsc::Receiver<JackNotification>) {
        self.jack_notification_rx = Some(rx);
    }
}
